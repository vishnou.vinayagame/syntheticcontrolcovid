import os
from PIL import Image
import pathlib
import re
import dash
import pandas as pd
import plotly.express as px
from dash import dcc
from dash import html
from plotly.subplots import make_subplots
from dash.dependencies import Input, Output, State
import cufflinks as cf
from figs_for_app import *

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.title = "US COVID-19 Incentives"
server = app.server

# l'appli est sur http://127.0.0.1:8050/
APP_PATH = str(pathlib.Path(__file__).parent.resolve())

DEFAULT_COLORSCALE = [
    "#f2fffb",
    "#bbffeb",
    "#98ffe0",
    "#79ffd6",
    "#6df0c8",
    "#69e7c0",
    "#59dab2",
    "#45d0a5",
    "#31c194",
    "#2bb489",
    "#25a27b",
    "#1e906d",
    "#188463",
    "#157658",
    "#11684d",
    "#10523e",
]

DEFAULT_OPACITY = 0.8

options_choix_type_donnees = [{'label': "Vaccination rates per day with states being clustered by votes for the 2016 presidential election", 'value': "Vaccination rates per day with states being clustered by votes for the 2016 presidential election"},
{'label': "US Map with states being clustered (k-Means) by votes for the 2016 presidential election", 'value': "US Map with states being clustered (k-Means) by votes for the 2016 presidential election"},
{'label': "Comparison of vaccination rates betweeen real and synthetic target states",'value':"Comparison of vaccination rates betweeen real and synthetic target states"},
{'label': "Feature importance of the parameters", 'value': "Feature importance of the parameters"},
{'label': "US Map of vaccination rates per month with states being clustered by votes for the 2016 presidential election", 'value':"US Map of vaccination rates per month with states being clustered by votes for the 2016 presidential election"},
{'label': "Gap between synthetic and real curves for 18-65 years old population", 'value':"Gap between synthetic and real curves for 18-65 years old population"},
{'label': "Gap between synthetic and real curves for over 65 years old population", 'value':"Gap between synthetic and real curves for over 65 years old population"},
{'label': "Placebo Test for Illinois - Distribution of gaps for control states", 'value':"Placebo Test for Illinois - Distribution of gaps for control states"}]



states_names = {"AL":"Alabama", "AK":"Alaska", "AZ":"Arizona", "AR":"Arkansas",
                "CA":"California", "CO":"Colorado", "CT":"Connecticut",
                "DE":"Delaware", "FL":"Florida", "GA":"Georgia", "HI":"Hawaii",
                "ID":"Idaho", "IL":"Illinois", "IN":"Indiana", "IA":"Iowa",
                "KS":"Kansas", "KY":"Kentucky", "LA":"Louisiana", "ME":"Maine",
                "MD":"Maryland", "MA":"Massachusetts", "MI":"Michigan",
                "MN":"Minnesota", "MS":"Mississippi", "MO":"Missouri", "MT":"Montana",
                "NE":"Nebraska", "NV":"Nevada", "NH":"New Hampshire", "NJ":"New Jersey",
                "NM":"New Mexico", "NY":"New York", "NC":"North Carolina",
                "ND":"North Dakota", "OH":"Ohio", "OK":"Oklahoma", "OR":"Oregon",
                "PA":"Pennsylvania", "RI":"Rhode Island", "SC":"South Carolina",
                "SD":"South Dakota", "TN":"Tennessee", "TX":"Texas", "UT":"Utah",
                "VT":"Vermont", "VA":"Virginia", "WA":"Washington", "WV":"West Virginia",
                "WI":"Wisconsin", "WY":"Wyoming"}

target_states_all = ['AL', 'AR', 'CA', 'CO', 'CT', 'DE', 'IL', 'ID', 'KY', 'ME', 'MD',
                'MI', 'MN', 'NJ', 'NY', 'OH', 'OR', 'WV']

parametres_names = ['Under 18 years old', 'Over 65 years old', 'Median age',
       'White', 'Black', 'American Indian', 'Asian', 'Hawaiian',
       'higher education achievement', 'high school achievement', 'literacy',
       'urban population', 'Population density (number of people/square mile)',
       'unemployement', 'Crime Rate', 'Firearms Death Rate', 'Poverty rate',
       'GINI index', 'Median income (in US dollars)', '2016 Trump vote',
       '2020 Trump vote', 'Governor: democrat (1) / republican (0)',
       'Gun ownership rate', 'Adult obesity rate', 'Hypertension',
       'Number of Covid-19 cases per 100k',
       'Number of Covid-19 deaths per 100k',
       'non-covered by a health insurance policy',
       'Covid vax hesitancy (somewhat hesitant)', 'Covid vax hesitancy (all)',
       'Access to Care (score between 0 and 1)']

options_param = [{'label':parametres_names[i], 'value':parametres_names[i]} for i in range(len(parametres_names))]

options_etats = [{'label':states_names[u], 'value':u} for u in target_states_all]

options_etats_incent = [{'label':states_names[u], 'value':states_names[u]} for u in target_states_all]

#le layout de l'app
app.layout = html.Div(
    
    children=[
    
        html.Div(
            id="header",
            children=[
                html.A(
                    html.Img(id="logo", src=app.get_asset_url("dash-logo.png")),
                    href="https://plotly.com/dash/",
                ),
                html.A(
                    html.Button("GitLab repository", className="link-button"),
                    href="https://gitlab-student.centralesupelec.fr/vishnou.vinayagame/synthetic_control_on_us_covid_19_incentives",
                ),
                html.A(
                    html.Button("Report", className="link-button"),
                    href="https://drive.google.com/file/d/1V1HNrZG0HR215nor0rOoEbqPGz77qaOY/view?usp=sharing",
                ),
                html.H4(children="Synthetic Control : COVID-19 Vaccination Incentives in USA"),
                html.P(
                    id="description",
                    children='Application of the Synthetic Control which is a statistical method used on time series to evaluate the efficiency of vaccination incentives applied in different states of the USA. Incentives can be grouped in 3 categories : Big prizes, Medium prizes and lotteries and Free gifts or small amounts of money. Our model is designed to predict the evolution of vaccination rates in a target state modeled by control states where the incentive have not been applied. The parameters of the model can be vizualised in the "Feature Importance" chart.',
                ),
            ],
        ),

        html.Div(
            id="app-container",
            children=[
                html.Div(
                    id="left-column",
                    children=[
                        html.Div(
                            id="heatmap-container", 
                            children=[
                                html.P(
                                    "List of incentives applied in the chosen state",
                                    id="graphique_gauche-title",
                                ),
                                dcc.Dropdown(
                                    id="dropdown_state_incentive_state",
                                    placeholder='Select a state',
                                    options=options_etats_incent,
                                    value="New Jersey"
                                ),
                                dcc.Graph(
                                    id="graph_de_gauche",
                                    figure=dict(
                                        layout=dict(
                                            paper_bgcolor="#1f2630",
                                            plot_bgcolor="#1f2630",
                                            autofill=True,
                                ),
                            ),
                        ),
                        ], style={'display': 'block', 'width': '95%'}
                        ),
                    ],
                ),
                html.Div(
                    id="graph-container",
                    children=[
                        html.P(id="chart-selector", children="Select chart:"),
                        
                        html.Br(),

                        dcc.Dropdown(
                            id='type_donnees',
                            options=options_choix_type_donnees,
                            placeholder='Select the chart',
                            value="Vaccination rates per day with states being clustered by votes for the 2016 presidential election"
                        ),

                        html.Br(),

                        dcc.Graph(
                            id="graphique",
                            figure=dict(
                                layout=dict(
                                    paper_bgcolor="#1f2630",
                                    plot_bgcolor="#1f2630",
                                    autofill=True,
                                ),
                            ),
                        ),
                        # Ajout de la liste déroulante pour choisir les données qu'on veut voir
                        html.Div(
                            id="slider-container",
                            children=[
                                html.P(
                                    id="slider-text",
                                    children="Drag the slider to change the month:",
                                ),
                                dcc.Slider(
                                    id='slider_date',
                                    min=0,
                                    max=300,
                                    step=30,
                                    value=90,
                                    marks={0: "Dec. 2020", 30: "Jan. 2021", 60: "Feb. 2021", 90: "Mar. 2021", 120: "Apr. 2021", 150: "May 2021", 180: "Jun. 2021", 210: "Jul. 2021", 240: "Aug. 2021", 270: "Sep. 2021", 300: "Oct. 2021"}
                                ),
                            ],
                        ),

                        dcc.Dropdown(
                            id='choix_etat',
                            options=options_etats,
                            placeholder="Select the target state you want to apply synthetic control to",
                            value="IL"
                        ),
                        html.Div(
                            id="dropdown-feature-b",
                            children=[
                                dcc.Dropdown(
                                    id='choix_param',
                                    options=options_param,
                                    placeholder="Select the feature of which you want to visualize the feature importance",
                                    value="White"
                                ),
                            ],
                        ),
                        # Ajout du slider pour sélectionner quel graphique on veut voir
                        html.Div(
                            id="slider-container-2",
                            children=[
                                html.P(
                                    id="slider-synthetic-text",
                                    children="Drag the slider to select the type of comparison:",
                                ),
                                dcc.Slider(
                                    id='slider',
                                    min=1,
                                    max=4,
                                    step=1,
                                    value=4,
                                    marks={1: 'Target vs Synthetic', 2: 'Target vs Synthetic (zoom)', 3: 'Target state vs Control states', 4: 'Target state vs Control states weighted'},
                                )
                            ],
                        ),
                        # Ajout du slider pour sélectionner la date du fit
                        html.Div(
                            id="slider-container-1",
                            children=[
                                html.P(
                                    id="slider_t0_fit-text",
                                    children="Drag the slider to select the timelength of the fit:",
                                ),
                                dcc.Slider(
                                    id='slider_t0_fit',
                                    min=30,
                                    max=60,
                                    step=10,
                                    value=40,
                                    marks={30: '30 days before the incentive', 40: '40 days before the incentive', 50: '50 days before the incentive', 60: '60 days before the incentive'},
                                )
                            ],
                        ),
                    ],
                ),
            ],
        ),
    ]
)

# Callbacks

# Callback pour l'update du graphe
@ app.callback(
    [Output('graphique', 'figure')],
    [Input('type_donnees', 'value')],[Input('choix_etat', 'value')],[Input('slider', 'value')],[Input('slider_t0_fit', 'value')],[Input('choix_param', 'value')],[Input('slider_date', 'value')])
def update_graph(type_donnees,choix_etat,slider,slider_t0_fit,choix_param, slider_date):

    if type_donnees == "Vaccination rates per day with states being clustered by votes for the 2016 presidential election":
        fig = comparaison_grps_kmeans()
    elif type_donnees == "US Map with states being clustered (k-Means) by votes for the 2016 presidential election":
        fig = carte_votes()

    elif type_donnees == "Comparison of vaccination rates betweeen real and synthetic target states":
        fig = synthetic_control_curves(choix_etat,slider,slider_t0_fit)

    elif type_donnees == "Feature importance of the parameters":
        fig = feature_importance(slider_t0_fit, choix_param)

    elif type_donnees == "US Map of vaccination rates per month with states being clustered by votes for the 2016 presidential election":
        fig = carte_vaxx(slider_date)

    elif type_donnees == "Gap between synthetic and real curves for 18-65 years old population":
        img = np.array(Image.open("assets/delta_1865.jpg"))

        fig = px.imshow(img, color_continuous_scale='gray')
        fig.update_layout(coloraxis_showscale=False)
        fig.update_xaxes(showticklabels=False)
        fig.update_yaxes(showticklabels=False)
        fig.update_layout(title = 'Gap between synthetic and real curves for 18-65 years old population at different times and for different start times')
        fig.update_layout(title={'y':0.9,'x':0.5,'xanchor': 'center','yanchor': 'top'})
        fig.update_layout(font=dict(size=7))

    elif type_donnees == "Gap between synthetic and real curves for over 65 years old population":
        img = np.array(Image.open("assets/delta_65.jpg"))

        fig = px.imshow(img, color_continuous_scale='gray')
        fig.update_layout(coloraxis_showscale=False)
        fig.update_xaxes(showticklabels=False)
        fig.update_yaxes(showticklabels=False)
        fig.update_layout(title = 'Gap between synthetic and real curves for over 65 years old population at different times and for different start times')
        fig.update_layout(title={'y':0.9,'x':0.5,'xanchor': 'center','yanchor': 'top'})
        fig.update_layout(font=dict(size=7))

    elif type_donnees == "Placebo Test for Illinois - Distribution of gaps for control states":

        img_name = "assets/placebo_ill_tfit" + str(slider_t0_fit) + ".jpg"


        img = np.array(Image.open(img_name))

        fig = px.imshow(img, color_continuous_scale='gray')
        fig.update_layout(coloraxis_showscale=False)
        fig.update_xaxes(showticklabels=False)
        fig.update_yaxes(showticklabels=False)
        fig.update_layout(margin=dict(l=20, r=20, t=20, b=20))

    return [fig]

# Callback pour l'update du graphe
@ app.callback(
    [Output('graph_de_gauche', 'figure')],
    [Input('dropdown_state_incentive_state', 'value')])
def update_graph_gauche(dropdown_state_incentive_state):

    fig = incentives_viz(dropdown_state_incentive_state)

    return [fig]

@app.callback(
    Output('choix_etat', 'style'),
    [Input('type_donnees', 'value')])
def set_app_choix_etat(type_donnees):
    if type_donnees == "Comparison of vaccination rates betweeen real and synthetic target states":
        return {'display': 'block'}
    else:
        return {'display': 'none'}

@app.callback(
    Output('choix_param', 'style'),
    [Input('type_donnees', 'value')])
def set_app_choix_param(type_donnees):
    if type_donnees == "Feature importance of the parameters":
        return {'display': 'block'}
    else:
        return {'display': 'none'}

@app.callback(
    Output('slider', 'style'),
    [Input('type_donnees', 'value')])
def set_app_choix_etat(type_donnees):
    if type_donnees == "Comparison of vaccination rates betweeen real and synthetic target states":
        return {'display': 'block'}
    else:
        return {'display': 'none'}

@app.callback(
    Output('slider_t0_fit', 'style'),
    [Input('type_donnees', 'value')])
def set_app_choix_etat(type_donnees):
    if type_donnees == "Comparison of vaccination rates betweeen real and synthetic target states" or type_donnees == "Feature importance of the parameters" or type_donnees == "Placebo Test for Illinois - Distribution of gaps for control states":
        return {'display': 'block'}
    else:
        return {'display': 'none'}

@app.callback(
    Output('slider-synthetic-text', 'style'),
    [Input('type_donnees', 'value')])
def set_app_choix_etat(type_donnees):
    if type_donnees == "Comparison of vaccination rates betweeen real and synthetic target states":
        return {'display': 'block'}
    else:
        return {'display': 'none'}


@app.callback(
    Output('slider-container', 'style'),
    [Input('type_donnees', 'value')])
def set_app_choix_etat(type_donnees):
    if type_donnees == "US Map of vaccination rates per month with states being clustered by votes for the 2016 presidential election":
        return {'display': 'block'}
    else:
        return {'display': 'none'}

@app.callback(
    Output('slider_t0_fit-text', 'style'),
    [Input('type_donnees', 'value')])
def set_app_choix_etat(type_donnees):
    if type_donnees == "Comparison of vaccination rates betweeen real and synthetic target states" or type_donnees == "Feature importance of the parameters":
        return {'display': 'block'}
    else:
        return {'display': 'none'}

@app.callback(
    Output('titre_slider_graph', 'style'),
    [Input('type_donnees', 'value')])
def set_app_choix_etat(type_donnees):
    if type_donnees == "Comparison of vaccination rates betweeen real and synthetic target states":
        return {'display': 'block'}
    else:
        return {'display': 'none'}

# Execution
if __name__ == '__main__':
    app.run_server(debug=False)
