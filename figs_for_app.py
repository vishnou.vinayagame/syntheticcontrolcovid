import datetime
from turtle import color, filling
from PIL import Image
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.optimize import minimize, fmin_slsqp
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.metrics import mean_squared_error
from yellowbrick.cluster import KElbowVisualizer
import plotly.express as px
import plotly.graph_objects as go
from plotly.tools import mpl_to_plotly
from plotly.subplots import make_subplots
from urllib.request import urlopen
import json
sns.set()

states = [ 'AK', 'AL', 'AR', 'AZ', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA',
           'HI', 'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME',
           'MI', 'MN', 'MO', 'MS', 'MT', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NM',
           'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX',
           'UT', 'VA', 'VT', 'WA', 'WI', 'WV', 'WY']

states_names = {"AL":"Alabama", "AK":"Alaska", "AZ":"Arizona", "AR":"Arkansas",
                "CA":"California", "CO":"Colorado", "CT":"Connecticut",
                "DE":"Delaware", "FL":"Florida", "GA":"Georgia", "HI":"Hawaii",
                "ID":"Idaho", "IL":"Illinois", "IN":"Indiana", "IA":"Iowa",
                "KS":"Kansas", "KY":"Kentucky", "LA":"Louisiana", "ME":"Maine",
                "MD":"Maryland", "MA":"Massachusetts", "MI":"Michigan",
                "MN":"Minnesota", "MS":"Mississippi", "MO":"Missouri", "MT":"Montana",
                "NE":"Nebraska", "NV":"Nevada", "NH":"New Hampshire", "NJ":"New Jersey",
                "NM":"New Mexico", "NY":"New York", "NC":"North Carolina",
                "ND":"North Dakota", "OH":"Ohio", "OK":"Oklahoma", "OR":"Oregon",
                "PA":"Pennsylvania", "RI":"Rhode Island", "SC":"South Carolina",
                "SD":"South Dakota", "TN":"Tennessee", "TX":"Texas", "UT":"Utah",
                "VT":"Vermont", "VA":"Virginia", "WA":"Washington", "WV":"West Virginia",
                "WI":"Wisconsin", "WY":"Wyoming"} 

parametres_names = ['Under 18 years old', 'Over 65 years old', 'Median age',
       'White', 'Black', 'American Indian', 'Asian', 'Hawaiian',
       'higher education achievement', 'high school achievement', 'literacy',
       'urban population', 'Population density (number of people/square mile)',
       'unemployement', 'Crime Rate', 'Firearms Death Rate', 'Poverty rate',
       'GINI index', 'Median income (in US dollars)', '2016 Trump vote',
       '2020 Trump vote', 'Governor: democrat (1) / republican (0)',
       'Gun ownership rate', 'Adult obesity rate', 'Hypertension',
       'Number of Covid-19 cases per 100k',
       'Number of Covid-19 deaths per 100k',
       'non-covered by a health insurance policy',
       'Covid vax hesitancy (somewhat hesitant)', 'Covid vax hesitancy (all)',
       'Access to Care (score between 0 and 1)']

#creation du dataframe des parametres
df_prm = pd.read_excel('parameters.xlsx', 'Sheet4', index_col=0)
df_prm = df_prm.drop(columns=['Population','Total Firearm Death','Number of people under poverty level'])
prm = df_prm.to_numpy()
    
#incentives pour la visualisation
df_incentives_viz = pd.read_excel('incentives_to_df.xlsx', 'Feuil1', index_col=0)

def incentives_viz(state_names):
    df_incentives_viz_s = df_incentives_viz.loc[state_names]

    fig = go.Figure(data=[go.Table(
        columnwidth = [800,800,800,800,800],
        header=dict(values=list(df_incentives_viz_s.columns),
                    fill_color='paleturquoise',
                    align='left'),
        cells=dict(values=[df_incentives_viz_s['Condition'], df_incentives_viz_s['Competition'], df_incentives_viz_s['Incentive'], df_incentives_viz_s['Start Date'], df_incentives_viz_s['Prize date/End date']],
                fill_color='lavender',
                align='center'))
    ])

    fig.update_layout(plot_bgcolor="#252e3f")


    return fig

state_vector = {}
for i in range(len(states)):
    state_vector[states[i]] = prm[i]

df_states = {}
for X in states:
  # fichiers csv à trouver dans le drive partagé
    path = "state_vax_csv/" + X + '.csv'
    df_states[X] = pd.read_csv(path, sep=',' )
    df_states[X] = df_states[X].loc[::-1].reset_index(drop=True)
    df_states[X] = df_states[X].loc[67:,:].reset_index()
    

# On s'intéresse à la colonne du pourcentage de personnes ayant reçu au moins une dose. 
# C'est notre "response variable" que l'on cherche à modéliser après l'intervention.

#On remarque qu'il y a des soucis dans le report des doses administrées : il arrive que le pourcentage de doses
#administrées diminue, alors qu'il ne devrait qu'augmenter ou rester constant (cf. explications dans le rapport)
#On décide donc de remédier à ces erreurs en fixant un seuil pour éviter que les données ne diminuent
#Dans un second temps, on effectue une interpolation sur la période où l'on constate des erreurs de report

tf = len(df_states['CA'])

for state in states:
    for t in range(1,tf):
        if df_states[state].loc[t, 'Administered_Dose1_Pop_Pct'] < df_states[state].loc[t-1, 'Administered_Dose1_Pop_Pct']:
            #on a un souci à partir du temps t
            j = t
            flag = True
            while j < tf and flag:
                if df_states[state].loc[j, 'Administered_Dose1_Pop_Pct'] >= df_states[state].loc[t-1, 'Administered_Dose1_Pop_Pct']:
                    #on a trouvé le plus petit indice pour lequel les données redeviennent cohérentes
                    a_t = df_states[state].loc[t-1, 'Administered_Dose1_Pop_Pct']
                    a_j = df_states[state].loc[j, 'Administered_Dose1_Pop_Pct']
                    
                    #on interpole linéairement
                    for i in range(t,j):
                        df_states[state].loc[i, 'Administered_Dose1_Pop_Pct'] = ((a_t-a_j)/(t-1-j))*(i-j) + a_j
                    flag = False
                j +=1 
                    
            if flag:
                #on a pas trouvé de plus petit indice: on crée un plateau à partir du temps t
                for i in range(t,tf):
                    df_states[state].loc[i, 'Administered_Dose1_Pop_Pct'] = df_states[state].loc[t-1, 'Administered_Dose1_Pop_Pct']

# On s'intéresse à la colonne du pourcentage de personnes ayant reçu au moins une dose. 
# C'est notre "response variable" que l'on cherche à modéliser après l'intervention.    
df_dose1 = {}
for X in states:
    df_dose1[X] = df_states[X].loc[:,'Administered_Dose1_Pop_Pct']

ls_dose1 = {}
for X in states:
    ls_dose1[X] = df_dose1[X].to_numpy()

#on normalise
df_prm_scaled = (df_prm-df_prm.mean())/df_prm.std()
prm_scaled = df_prm_scaled.to_numpy()

state_vector_scaled = {}
for i in range(len(states)):
    state_vector_scaled[states[i]] = prm_scaled[i]

#On applique un k-Means

# Nombre de groupes
n = 3

km = KMeans(
    n_clusters= n, init='random',
    n_init=10, max_iter=300, 
    tol=1e-05, random_state=0
)

y_km = km.fit_predict(df_prm_scaled)

states_A, states_B, states_C= [], [], []
states_A_names, states_B_names, states_C_names = [], [], []
for i in range(len(states)):
    if y_km[i] == 0:
        states_A_names.append(states_names[states[i]])
        states_A.append(states[i])
    elif y_km[i] == 1:
        states_B_names.append(states_names[states[i]])
        states_B.append(states[i])
    elif y_km[i] == 2 and n >= 3:
        states_C_names.append(states_names[states[i]])
        states_C.append(states[i])

#On définit les target states et les control states
target_states_all = ['AL', 'AR', 'CA', 'CO', 'CT', 'DE', 'IL', 'ID', 'KY', 'ME', 'MD',
                'MI', 'MN', 'NJ', 'NY', 'OH', 'OR', 'WV']
control_states_all = []

target_states = {'A': [], 'B': [], 'C':[]}
control_states = {'A': [], 'B': [], 'C':[]}

for X in states:
      if X not in target_states_all:
        control_states_all.append(X)

for X in states:
    if X in states_A and X in target_states_all:
        target_states['A'].append(X)
    elif X in states_A and X not in target_states_all:
        control_states['A'].append(X)
    elif X in states_B and X in target_states_all:
        target_states['B'].append(X)
    elif X in states_B and X not in target_states_all:
        control_states['B'].append(X)
    elif X in states_C and X in target_states_all:
        target_states['C'].append(X)
    elif X in states_C and X not in target_states_all:
        control_states['C'].append(X)

# Interprétation des groupes
# Remarque: même en supprimant les paramètres "politiques" (vote Trump 2020, 2016, ...)
#           le clustering génère les mêmes groupes. 


vote_A, vote_B, vote_C= [], [], []
states_A = np.concatenate((target_states['A'], control_states['A']))
states_B = np.concatenate((target_states['B'], control_states['B']))
states_C = np.concatenate((target_states['C'], control_states['C']))

for X in states_A:
    if df_prm.loc[X, "2016 Trump vote"] >= 0.5:
        vote_A.append(1)
    else:
        vote_A.append(0)       
pct_A = 100*np.sum(vote_A)/len(vote_A)

for X in states_B:
    if df_prm.loc[X, "2016 Trump vote"] >= 0.5:
        vote_B.append(1)
    else:
        vote_B.append(0)
pct_B = 100*np.sum(vote_B)/len(vote_B)

for X in states_C:
    if df_prm.loc[X, "2016 Trump vote"] >= 0.5:
        vote_C.append(1)
    else:
        vote_C.append(0)
pct_C = 100*np.sum(vote_C)/len(vote_C)


def comparaison_grps_kmeans():
    mpl_fig = plt.figure()

    for X in states:
        c = 0
        if X in states_A:
            c = 'b'
        elif X in states_B:
            c = 'r'
        elif X in states_C:
            c = 'y'

        t = np.linspace(1,len(ls_dose1[X]), len(ls_dose1[X]))

        plt.plot(t, ls_dose1[X], color=c, label = states_names[X])
 
    fig = mpl_to_plotly(mpl_fig)
    fig.update_layout(xaxis_title="Time in days", title = 'Group A [Blue] vs Group B [Red] vs Groupe C [Yellow]', yaxis_title = "% of 1st dose vaccinations")
    fig.update_layout(width=600, height=400)

    return fig

def carte_votes():

    # Carte 
    c_list = []
    for X in states:
        if X in states_A:
            c_list.append(1)
        elif X in states_B:
            c_list.append(2)
        elif X in states_C:
            c_list.append(3)
    
    df_colors = pd.DataFrame({"Group" : c_list})
    df_colors = df_colors.rename(index={i:states[i] for i in range(len(states))})

    fig = px.choropleth(df_colors,locations=states, locationmode="USA-states", color="Group", scope="usa")
    fig.update_layout(geo=dict(bgcolor= "#1f2630"))
    fig.update_layout(margin=dict(l=0, r=0, t=0, b=0))
    fig.update_layout(width=600, height=500)
    fig.update_layout(font=dict(size=10))
    fig.update_layout(title = 'Group A [Blue] vs Group B [Red] vs Groupe C [Yellow]')
    fig.add_annotation(showarrow=False, xref="paper", yref="paper", x=0.5, y=0.14,text="k-Means clustering according to votes for the 2016 presidential election")
    fig.add_annotation(showarrow=False, xref="paper", yref="paper", x=0.5, y=0.10, text="% states that voted Trump in 2016 in group A: "+str(round(pct_A))+"%")
    fig.add_annotation(showarrow=False, xref="paper", yref="paper", x=0.5, y=0.06, text="% states that voted Trump in 2016 in group B: "+str(round(pct_B))+"%")
    fig.add_annotation(showarrow=False, xref="paper", yref="paper", x=0.5, y=0.02, text="% states that voted Trump in 2016 in group C: "+str(round(pct_C))+"%")

    fig.update_layout(coloraxis_showscale=False)
    fig.update_layout(title={'y':0.9,'x':0.5,'xanchor': 'center','yanchor': 'top'})
    
    return fig


def carte_vaxx(t):
    
    # Carte 
    c_list = []
    for X in states:
        u = df_states[X].loc[t, 'Administered_Dose1_Pop_Pct'] 
        c_list.append(u) 
    
    df_colors = pd.DataFrame({"Vaccination rates in %" : c_list})
    df_colors = df_colors.rename(index={i:states[i] for i in range(len(states))})

    fig = px.choropleth(df_colors,title = "US Map of vaccination rates per month with states being clustered by votes for the 2016 presidential election",locations=states, locationmode="USA-states", color="Vaccination rates in %", scope="usa")
    fig.update_layout(geo=dict(bgcolor= "#1f2630"))
    fig.update_layout(margin=dict(l=0, r=0, t=0, b=0))
    fig.update_layout(title={"yref": "paper", "y" : 0.1,"yanchor" : "bottom"})
    fig.update_layout(coloraxis_showscale=False)
    fig.update_layout(font=dict(size=10))
    fig.update_layout(width=600, height=500)
    
    return fig


#les incentives
incentives = pd.read_excel('incentive_list.xlsx', 'Sheet2', index_col=None)

dates = {}
category = {}
for X in target_states_all:
    dates[X] = incentives.loc[incentives["State Code"] == X, "Incentive t0"]
    dates[X] = str(dates[X].item())[:10]
    category[X] = incentives.loc[incentives["State Code"] == X, "Category"]
    category[X] = int(category[X])

#Principal component analysis
pca = PCA().fit(df_prm_scaled)


u, s, v = np.linalg.svd(df_prm_scaled, full_matrices=True)

v10 = v[:5,:]

var_explained = np.round(s**2/np.sum(s**2), decimals=3)

prm_scaled_t = np.transpose(prm_scaled)
prm_pca = np.transpose(np.dot(v10, prm_scaled_t))

state_vector_pca = {}
for i in range(len(states)):
    state_vector_pca[states[i]] = prm_pca[i]

#PCR : à faire

#Retour sur le synthetic control

def norm(w, v, x, x1):
    pred = np.dot(np.transpose(x), w)
    pred_diff = x1 - pred
    pred_diff_v = np.multiply(pred_diff, v)
    res = np.dot(pred_diff, pred_diff_v)
    return np.sqrt(res)

def mspe(y1, y, w):
    interv = np.dot(np.transpose(y), w)
    interv_diff = y1 - interv
    res = np.dot(interv_diff, interv_diff)
    return res

def constraint(w, v, x, x1):
    return np.sum(w) - 1

def con(v):
    return np.sum(v) - 1
cons = {'type':'eq', 'fun': con}

def mspe_opt_w(v_guess, w_guess, x, x1, y, y1):
    w_opt = fmin_slsqp(norm, w_guess, f_eqcons=constraint, bounds=[(0.0, 1.0)]*len(w_guess),
             args=(v_guess, x, x1), disp=False)
    return mspe(y1, y, w_opt)

def opt_v(v_guess, w_guess, x, x1, y, y1):
    v_opt = minimize(mspe_opt_w, v_guess, constraints = cons, bounds=[(0.0, 1.0)]*len(v_guess), 
             args=(w_guess, x, x1, y, y1))
    return v_opt.x

def opt_w(w_guess, v, x, x1):
    w_opt = fmin_slsqp(norm, w_guess, f_eqcons=constraint, bounds=[(0.0, 1.0)]*len(w_guess),
             args=(v, x, x1), disp=False)
    return w_opt

#Temps t0
inc_t0 = {}

for X in target_states_all:
    # 19/02/2021 approx première date à laquelle les % de vax sont non nuls
    start = datetime.date(2021,2,19)
    t0_date_str = dates[X]
    t0_date = datetime.date(int(t0_date_str[:4]), int(t0_date_str[5:7]), int(t0_date_str[9:]))
    t0 = t0_date - start
    t0 = t0.days
    inc_t0[X] = t0


#Calcul des synthetic

dfs={}
dfs['A'] = pd.DataFrame(columns=control_states['A'])
dfs['B'] = pd.DataFrame(columns=control_states['B'])
dfs['C'] = pd.DataFrame(columns=control_states['C'])

#Mettre ça en global et une fonction juste pour sortir les courbes

def synthetic(t_fit):
    
    V = []
    y1_prediction = {}
    w_star_global = {}

    for Y in target_states_all:

        X_target = Y
        
        t0 = inc_t0[X_target]
        
        if X_target in states_A:
            L = 'A'
        elif X_target in states_B:
            L = 'B'
        elif X_target in states_C:
            L = 'C'

        y1 = ls_dose1[X_target][t0-t_fit:t0]
        y = []
        for X in control_states_all:    
            y.append(ls_dose1[X][t0-t_fit:t0])

        y_post = []
        for X in control_states_all:    
            y_post.append(ls_dose1[X])

        #   Choisir ici state_vector_scaled (35 paramètres normalisés) 
        #   ou state_vector_pca (5 principaux paramètres du PCA).
        #       Il semble que les 35 paramètres normalisés génèrent un synthetic 
        #       plus proche du réel sur la période avant l'intervention.
        x1 = state_vector_scaled[X_target]

        x = []
        for X in control_states_all :    
                x.append(state_vector_scaled[X])

        k = len(x)
        j = len(x1)

        w_guess = np.array([1/k]*k)
        v_guess = np.array([1/j]*j)
    

        v_star = opt_v(v_guess, w_guess, x, x1, y, y1)
        w_star = opt_w(w_guess, v_star, x, x1)
        V.append(np.concatenate(([L], np.round(v_star, 3))))

        y1_prediction[X_target] = np.dot(np.transpose(y_post), w_star)

        df_new_row = pd.DataFrame(data=np.array([w_star]), columns=control_states_all)

        dfs[L]= pd.concat([dfs[L],df_new_row], ignore_index=True)

        inf = 0.01
        states_star = {}

        for k in range(len(w_star)):
            if w_star[k]>inf:
                states_star[control_states_all[k]] = w_star[k]
        
        w_star_global[X_target] = w_star

    return V, y1_prediction,w_star_global

poids_v30 = pd.read_csv("Résultats/poids_v30.csv")
poids_v40 = pd.read_csv("Résultats/poids_v40.csv")
poids_v50 = pd.read_csv("Résultats/poids_v50.csv")
poids_v60 = pd.read_csv("Résultats/poids_v60.csv")

y1_prediction30 = pd.read_csv("Résultats/y1_prediction30.csv").to_dict()
y1_prediction40 = pd.read_csv("Résultats/y1_prediction40.csv").to_dict()
y1_prediction50 = pd.read_csv("Résultats/y1_prediction50.csv").to_dict()
y1_prediction60 = pd.read_csv("Résultats/y1_prediction60.csv").to_dict()

del poids_v30['Unnamed: 0']
del poids_v40['Unnamed: 0']
del poids_v50['Unnamed: 0']
del poids_v60['Unnamed: 0']

del y1_prediction30['Unnamed: 0']
del y1_prediction40['Unnamed: 0']
del y1_prediction50['Unnamed: 0']
del y1_prediction60['Unnamed: 0']

w_star_global30 = pd.read_csv("Résultats/w_star_global30.csv").to_dict()
w_star_global40 = pd.read_csv("Résultats/w_star_global40.csv").to_dict()
w_star_global50 = pd.read_csv("Résultats/w_star_global50.csv").to_dict()
w_star_global60 = pd.read_csv("Résultats/w_star_global60.csv").to_dict()

for X_target in target_states_all:
    y1_prediction30[X_target] = np.array(list(y1_prediction30[X_target].values()))
    y1_prediction40[X_target] = np.array(list(y1_prediction40[X_target].values()))
    y1_prediction50[X_target] = np.array(list(y1_prediction50[X_target].values()))    
    y1_prediction60[X_target] = np.array(list(y1_prediction60[X_target].values()))

    w_star_global30[X_target] = list(w_star_global30[X_target].values())
    w_star_global40[X_target] = list(w_star_global40[X_target].values())
    w_star_global50[X_target] = list(w_star_global50[X_target].values())
    w_star_global60[X_target] = list(w_star_global60[X_target].values())

def synthetic_control_curves(target_state, cat_subplot,t_fit):
    
    X_target = target_state

    def arg_synthetic_curves(t_fit):
        if t_fit == 30:
            return poids_v30,y1_prediction30,w_star_global30
        elif t_fit == 40:
            return poids_v40,y1_prediction40,w_star_global40
        elif t_fit == 50:
            return poids_v50,y1_prediction50,w_star_global50
        else:
            return poids_v60,y1_prediction60,w_star_global60

    V, y1_prediction, w_star_global = arg_synthetic_curves(t_fit)

    w_star = w_star_global[X_target]

    t = np.linspace(1,len(ls_dose1[X_target]), len(ls_dose1[X_target]))
    t_zoom = np.linspace(t0-40,t0+70, 110)
    
    if X_target in states_A:
        L = 'A'
    elif X_target in states_B:
        L = 'B'
    elif X_target in states_C:
        L = 'C'

    #l'affichage

    mpl_fig = plt.figure()

    if cat_subplot == 1:
        plt.plot(t, ls_dose1[X_target], label='real')
        plt.plot(t, y1_prediction[X_target], label='synthetic')
        lbl = "t0=" + t0_date_str
        fig = mpl_to_plotly(mpl_fig)
        fig.update_layout(title = states_names[X_target]+ ' (Groupe '+L+'| Category '+str(category[X_target])+')')
        fig.add_vline(x=t0, line_width=3, line_dash="dash", line_color="green")
        fig.add_vline(x=t0-t_fit, line_width=3, line_dash="dash", line_color="red", annotation_text= "start of the fit")


    elif cat_subplot == 2:
        plt.plot(t_zoom, ls_dose1[X_target][t0-40:t0+70], label='real')
        plt.plot(t_zoom, y1_prediction[X_target][t0-40:t0+70], label='synthetic')
        lbl = "t0=" + t0_date_str
        fig = mpl_to_plotly(mpl_fig)
        fig.update_layout(title = states_names[X_target] + ' [zoom] (Groupe '+L+'| Category '+str(category[X_target])+')')
        fig.add_vline(x=t0, line_width=3, line_dash="dash", line_color="green")
        fig.add_vline(x=t0-t_fit, line_width=3, line_dash="dash", line_color="red", annotation_text= "start of the fit")


    elif cat_subplot == 4:
        for i in range(len(control_states[L])):
            X = control_states[L][i]
            if X != X_target:
                th = 2*w_star[i]
                plt.plot(t, ls_dose1[X], 'g', linewidth=th, label = states_names[X])
        plt.plot(t, ls_dose1[X_target], label='real')
        plt.plot(t, y1_prediction[X_target], label='synthetic')
        lbl = "t0=" + t0_date_str
        fig = mpl_to_plotly(mpl_fig)
        fig.update_layout(title = states_names[X_target]+' vs final control states')
        fig.add_vline(x=t0, line_width=3, line_dash="dash", line_color="green")
        fig.add_vline(x=t0-t_fit, line_width=3, line_dash="dash", line_color="red", annotation_text= "start of the fit")


    else:
        for X in control_states[L]:
            if X != X_target:
                plt.plot(t, ls_dose1[X], 'b--', linewidth=0.5, label = states_names[X])
        plt.plot(t, ls_dose1[X_target], label='real')
        plt.plot(t, y1_prediction[X_target], label='synthetic')
        lbl = "t0=" + t0_date_str
        fig = mpl_to_plotly(mpl_fig)
        fig.update_layout(title = states_names[X_target]+' vs Groupe '+L)
        fig.add_vline(x=t0, line_width=3, line_dash="dash", line_color="green")

    fig.update_layout(width=600, height=400)

    return fig

def fit_q(ls_dose, y1_prediction, after_t0=False):
    fit_quality = []

    if after_t0 == False:
        for X in target_states_all:
            t0 = inc_t0[X]
            err = mean_squared_error(ls_dose1[X][:t0],y1_prediction[X][:t0])
            fit_quality.append(np.round(np.sqrt(err),2))
    else:
        for X in target_states_all:
            t0 = inc_t0[X]
            err = mean_squared_error(ls_dose1[X][t0+1:],y1_prediction[X][t0+1:])
            fit_quality.append(np.round(np.sqrt(err),2))

    df_fit = pd.DataFrame(fit_quality, index = target_states_all, columns=['%'])

    return df_fit

time_delta = [0, 30, 60, 90]

def delta_y(ls_dose, y1_prediction):
  delta = []

  for X in target_states_all:
    t0 = inc_t0[X]
    delta += [[np.round(y1_prediction[X][t0+d] -ls_dose1[X][t0+d],2) for d in time_delta]]

  df_delta = pd.DataFrame(delta, index = target_states_all, columns = time_delta)

  return df_delta

# Analyse de l'importance respective des coefficients des paramètres V*

def feature_importance(t_fit, selected_feature):
    
    def arg_feat_imp(t_fit):
        if t_fit == 30:
            return poids_v30
        elif t_fit == 40:
            return poids_v40
        elif t_fit == 50:
            return poids_v50
        else:
            return poids_v60
            
    poids_v1 = arg_feat_imp(t_fit).drop(columns=['Groupe'])
    poids_v1 = poids_v1.astype('float64')
    
    fig = px.bar(poids_v1, x=target_states_all, y=selected_feature)
    fig.update_xaxes(type='category')

    fig.update_layout(width=600, height=400)
    fig.update_layout(title = 'Feature importance of ' + selected_feature)

    return fig

