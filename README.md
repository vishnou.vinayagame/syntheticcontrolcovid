# Synthetic Control On US Covid-19 Incentives

Application of the Synthetic Control which is a statistical method used on time series to evaluate the efficiency of vaccination incentives applied in different states of the USA.

Project Team :
Hector BONNEFOI
Théodore DE POMEREU
Lucas LEFORESTIER
Antoine LOPES FERREIRA
Vishnou VINAYAGAME

Supervisor :
Marie-Laure Charpignon

Lauch dataviz_app.py in your favorite Python interpreter, and go on:
http://127.0.0.1:8050/
Make sure you have all the requirements satisfied and enjoy the vizualisation !
